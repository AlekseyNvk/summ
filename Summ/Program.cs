﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Summ
{
    public class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Please enter path to processing folder:");
            string folderPath = Console.ReadLine();

            Console.WriteLine("Please enter name for file. That file will be used for save result.");
            Console.WriteLine("If this file alredy exist we try to overwrite this file");
            string outputfileName = Console.ReadLine();

            var ByteSumms = await CalculateSummsForfilesInFolderIncludeDeepFiles(folderPath,
                new Progress<FileProcessed>(
                    (f) => Console.WriteLine($"{f.FilePath} : {f.SummOfBytes}")));

            SaveFile(GetReportAsXml(ByteSumms), folderPath, outputfileName);
        }

        #region SummCalc
        private const int EOF = -1;
        private const long calculatedError = -1L;

        public async static Task<IReadOnlyCollection<FileProcessed>> CalculateSummsForfilesInFolderIncludeDeepFiles(
            string folderPath, IProgress<FileProcessed> progressNotifications)
        {
            return await 
                Task.Run(() => Directory.EnumerateFiles(folderPath, "*", SearchOption.AllDirectories)
                .AsParallel()
                .Select((filePath) => CalculateSummOfBytesForFile(filePath, progressNotifications))
                .ToList().AsReadOnly());
        }

        public static FileProcessed CalculateSummOfBytesForFile(
            string filePath, IProgress<FileProcessed> progressNotifications)
        {
            FileProcessed returnValue = null;
            try
            {
                using(var file = new FileStream(filePath, FileMode.Open))
                {
                    long summOfBytes = CalculateSummOfBytesForStream(file);
                    return returnValue = new FileProcessed(filePath, summOfBytes);
                }
            }
            catch
            {
                return returnValue = new FileProcessed(filePath, calculatedError);
            }
            finally
            {
                progressNotifications?.Report(returnValue);
            }
        }

        public static long CalculateSummOfBytesForStream(Stream stream)
        {
            int buff = 0;
            long summ = 0;

            while((buff = stream.ReadByte()) != EOF)
            {
                summ += buff;
            }

            return summ;
        }
        #endregion SummCalc

        #region SaveAsXml
        public static XDocument GetReportAsXml(IReadOnlyCollection<FileProcessed> fileProcesseds)
        {
            return new XDocument(
                new XElement("Root",
                    fileProcesseds.Select(file =>
                        new XElement("File",
                            new XElement("FilePath", file.FilePath),
                            new XElement("SummOfBytes", file.SummOfBytes)))));
        }

        public static void SaveFile(XDocument xDocument, string folderPath, string fileName)
        {
            string outputFilePath = Path.Combine(folderPath, fileName);
            using(var outputfile = new FileStream(outputFilePath, FileMode.Create))
            {
                xDocument.Save(outputfile);
            }

        }
        #endregion SaveAsXml
    }
}