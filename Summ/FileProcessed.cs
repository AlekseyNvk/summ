﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Summ
{
    public sealed class FileProcessed
    {
        public readonly string FilePath;
        public readonly long SummOfBytes;

        public FileProcessed(string filePath, long summOfBytes)
        {
            FilePath = filePath;
            SummOfBytes = summOfBytes;
        }
    }
}
