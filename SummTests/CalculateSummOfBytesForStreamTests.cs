using NUnit.Framework;
using Summ;
using System.IO;

namespace SummTests
{
    public class CalculateSummOfBytesForStreamTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void WhenPassEmptyStream_ShouldSummOfBytesMustBeZero()
        {
            var emptyStream = new MemoryStream(new byte[] { });

            var summ = Summ.Program.CalculateSummOfBytesForStream(emptyStream);

            Assert.That(summ == 0);
        }

        [TestCase(new byte[] { 0, 0, 0 }, 0)]
        [TestCase(new byte[] { 1, 5, 1 }, 7)]
        [Test]
        public void WhenPassStream_SholdSummOfBytesMustBeCorrect(byte[] bytes, long correctSumm)
        {
            var stream = new MemoryStream(bytes);

            var summ = Summ.Program.CalculateSummOfBytesForStream(stream);

            Assert.That(summ == correctSumm);
        }
    }
}